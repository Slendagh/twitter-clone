<?php

use Illuminate\Support\Facades\Route;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;
use App\Exports\TweetsExport;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('sign-in/github', 'Auth\LoginController@github');
Route::get('/sign-in/github/redirect', 'Auth\LoginController@githubRedirect');

/*use Illuminate\Support\Facades\Mail;
Route::get('/email', function () {
    Mail::to('az.asmaa.az@gmail.com')->send(new WelcomeMail());
    return new WelcomeMail();
});*/

Auth::routes(['verify' => true]);
Route::get('/home', 'TweetController@index')->name('home')->middleware('verified');
Route::get('/logout', 'Auth\LoginController@logout');

Route::post('tweet/store', 'TweetController@store');
Route::get('/tweet/comment', ['uses' =>'TweetController@showTweet'])->name('tweet/comment');
Route::get('/tweet/comment/showReplys', ['uses' =>'TweetController@showReplys'])->name('tweet/comment/showReplys');
Route::post('/tweet/likes',  ['uses' =>'TweetController@storeLikes'])->name('tweet/likes');
Route::post('/tweet/retweets',  ['uses' =>'TweetController@storeRetweets'])->name('tweet/retweets');
Route::post('/tweet/delete',  ['uses' =>'TweetController@destroy'])->name('tweet/delete');
Route::post('/tweet/statistics',['uses' =>'TweetController@statistics'])->name('tweet/statistics'); 


Route::get('/profile/{id}', 'TweetController@showProfile')->name('profile/{id}')->middleware('verified');

Route::get('/downloadUsers', function(){
    Excel::store(new UsersExport, 'users.csv');
    return "download complete";
});

Route::get('/downloadTweets', function(){
    return Excel::download(new TweetsExport, 'tweets.csv');
    //return "download complete";
});

Route::get('AutoCompleteUsers/index', ['uses' =>'AutoCompleteUsers@index'])->name('AutoCompleteUsers/index');

//php intervention package
Route::get('/img', function()
{
    $img = Image::make('https://miro.medium.com/max/700/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg')->insert('https://img.icons8.com/plasticine/2x/star.png', 'bottom-right');

    return $img->response('jpg');
});