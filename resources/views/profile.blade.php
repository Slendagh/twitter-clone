@extends('layouts.app')

@section ('content')
<style>
    body{
        background-color:#141f27;
    }
    </style>
<div class="container pl-2 pr-2">
    <div class="row">
        <div id="left" class="col" style="width:100%;" >
            <div class="col" style="position: fixed; width:100%;">
                <div class="d-flex flex-column justify-content-center" style="width:fit-content; height:100vh;position: relative ">
                    <a href="/home"><img src="{{url('images/twitter-logo.jpg')}}" style="position: fixed;top: 0;bottom: 0;margin: 1em;width:50px; height:50px;"></a>
                    <button class="noHover btn btn-dark" style="margin-bottom:2em;width:12em;"><a href="/home" ><i class="fa fa-home" aria-hidden="true"></i> Home</a></button>
                    <button class="btn btn-dark" style="margin-bottom:2em; width:12em;"><a href="{{url('/profile/'.Auth::User()->id.'')}}"><i class="fa fa-user" aria-hidden="true"></i> Profile</a></button>
                    <button class="btn btn-primary rounded-pill" style="margin-bottom:2em; width:12em;" data-toggle="modal" data-target="#myModal">Tweet</button>
                    <div class="dropup" style="margin-left:0em;position: absolute;bottom: 0;left: 0;margin-bottom: 4em; width:12em;" >
                        <button class="btn btn-dark dropdown-toggle"  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            <div class="d-flex flex-row" >
                                <img src="{{asset('storage/'.Auth::User()->avatar.'')}}" class="rounded-circle" alt="Cinque Terre" style="width: 3.5em;height: 3.3em;">
                                <div style="width:7em;">
                                    <div style="color:white; width:fit-content; padding-left:2em;">{{ Auth::User()->name }}</div>
                                    <div style="color:#AAB8C2; width:fit-content; padding-left:2em;"> {{Auth::User()->username}}</div>
                                </div>
                            </div>
                        </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <li><a class="dropdown-item" href="{{ route('logout') }}">{{ __('log out') }}</a></li>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="middle" class="col-7  border-bottom-0 border border-secondary" style="padding:0;">
            <div id="nav" class="sticky-top border-bottom border-secondary" >
                <a href="#" ><Strong>{{ $results[0]->user->name }}</strong></a>
            </div>
            <div class="avatar-tweeto" style="display: flex; align-items: center;flex-direction: column;">
                <img src="{{asset('storage/'.$results[0]->user->avatar.'')}}" class="rounded-circle" alt="Cinque Terre" style="width: 15em;height: 14em;">
                <div style="margin-top:2em;"> {{$results[0]->user->username}}</div>
            </div>
            <div class="border-bottom-0 border border-secondary" style="padding:0;"></div>
            <!--all the tweets-->
            <div class="tweet">
            @foreach($results as $result)
                <div class="tweet-container" style="padding: 1em;"> 
                    <div class="avatar-tweeto">
                        <img src="{{asset('storage/'.$result->user->avatar.'')}}" class="rounded-circle" alt="Cinque Terre" style="width: 3.5em;height: 3.3em;">
                    </div>
                    <div class="tweet-body">
                        <span style="color:white;">{{ $result->user->name }}</span><span style="color:#AAB8C2;"> {{$result->user->username}} <span style="font-size: smaller;">{{ $result->created_at->diffForHumans() }}</span></span>
                        <span onclick="deleteTweet({{$result->id}});" style="float:right; cursor: pointer;"><i class="fa fa-trash-o" aria-hidden="true"></i></span>
                            <div class="tweet-text">
                                <span style="color:white;">{{$result->text}}</span>
                                <div class="tweet-interaction">
                                    <span style="cursor: pointer;" data-toggle="modal" data-id="{{$result->id}}" class="open-modal" onClick='setFocus();'><i class="fa fa-comment-o" aria-hidden="true"></i></span>
                                @if($result->likes == 0)
                                    <span ><input  class="heart" id="{{$result->created_at}}" type="checkbox" /><label for="{{$result->created_at}}"><i class="fa fa-heart-o" onclick="addLike({{$result->id}});" aria-hidden="true" style="cursor: pointer;"></i></label> <span id="{{$result->id}}"></span></span>
                                @else
                                    <span ><input  class="heart" id="{{$result->created_at}}" type="checkbox" /><label for="{{$result->created_at}}"><i class="fa fa-heart-o" onclick="addLike({{$result->id}});" aria-hidden="true" style="cursor: pointer;"></i></label> <span id="{{$result->id}}">{{$result->likes}}</span></span>
                                @endif
                                </div>
                            </div>
                    </div>
                </div>
                <div class="border-bottom-0 border border-secondary" style="padding:0;"></div>
                @endforeach
            </div>
            <!--to be displayed only on mobile-->
            <div id="mobileTweetBtn" style="position: fixed;bottom: 2em;left: 78%;">
                <button class="btn btn-primary btn-circle" data-toggle="modal" data-target="#myModal">Tweet</button>
            </div>
            <!--end of mobile feature-->
        </div>
        <div id="right" class="col">
            <p>There are no news here, just corona TIME !</p>
        </div>
    </div>
</div>


<!-- tweet Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="background-color:#141f27">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
            <form action="{{url('tweet/store')}}" method="POST">
                @csrf
                <div class="whatsup">
                    <div class="avatar">
                        <img src="{{asset('storage/'.Auth::User()->avatar.'')}}" class="rounded-circle" alt="Cinque Terre" style="width: 3.5em;height: 3.3em;">
                    </div>
                    <div style="padding: 1em;width:100%;">
                        <textarea name="text" placeholder="What's happening?" style="color:white;"></textarea>
                    </div>
                </div>
                <button class="btn btn-primary" type="submit" style="float:right;margin-right: 1em;">Tweet</button><br><br>
            </form>
        </div>
    </div>

  </div>
</div>

<script type="text/javascript">
//to add likes => increment
function addLike(tweetId){
    $.ajax({
        type : 'post',
        headers: {'X-CSRF-Token': '{{ csrf_token() }}',},
        url : "{{url('tweet/likes')}}", //Here you will fetch records 
        datatype: 'JSON',
        data :  'id='+ tweetId, //Pass $id
        success : function(data){
            console.log("nb tweets: " + data);
            $('#'+tweetId).html(data);
        }
    }); 
    console.log("id; "+tweetId);
}
//delete tweet
function deleteTweet(tweetId){
    $.ajax({
                type : 'post',
                headers: {'X-CSRF-Token': '{{ csrf_token() }}',},
                url : "{{url('tweet/delete')}}", //Here you will fetch records 
                datatype: 'JSON',
                data :  'id='+ tweetId, //Pass $id
                success : function(data){
                    console.log(data);
                    location.reload();
                }
            }); 
            console.log(tweetId);
}
</script>
@endsection
