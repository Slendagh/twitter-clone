@extends('layouts.app')

@section ('content')

<style>
    body{
        background-color:#141f27;
    }
    </style>
<div class="container pl-2 pr-2">
    <div class="row">
        <div id="left" class="col" style="width:100%;" >
            <div class="col" style="position: fixed; width:100%;">
                <div class="d-flex flex-column justify-content-center" style="width:fit-content; height:100vh;position: relative ">
                    <a href="/home"><img src="{{url('images/twitter-logo.jpg')}}" style="position: fixed;top: 0;bottom: 0;margin: 1em;width:50px; height:50px;"></a>
                    <button class="noHover btn btn-dark" style="margin-bottom:2em;width:12em;"><a href="/home" ><i class="fa fa-home" aria-hidden="true"></i> Home</a></button>
                    <button class="btn btn-dark" style="margin-bottom:2em; width:12em;"><a href="{{url('/profile/'.Auth::User()->id.'')}}"><i class="fa fa-user" aria-hidden="true"></i> Profile</a></button>
                    <button class="btn btn-primary rounded-pill" style="margin-bottom:2em; width:12em;" data-toggle="modal" data-target="#myModal">Tweet</button>
                    <div class="dropup" style="margin-left:0em;position: absolute;bottom: 0;left: 0;margin-bottom: 4em; width:12em;" >
                        <button class="btn btn-dark dropdown-toggle"  type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                            <div class="d-flex flex-row" >
                                <img src="{{asset('storage/'.Auth::User()->avatar.'')}}" class="rounded-circle" alt="Cinque Terre" style="width: 3.5em;height: 3.3em;">
                                <div style="width:7em;">
                                    <div style="color:white; width:fit-content; padding-left:2em;">{{ Auth::User()->name }}</div>
                                    <div style="color:#AAB8C2; width:fit-content; padding-left:2em;"> {{Auth::User()->username}}</div>
                                </div>
                            </div>
                        </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <li><a class="dropdown-item" href="{{ route('logout') }}">{{ __('log out') }}</a></li>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="middle" class="col-md-7  border-bottom-0 border border-secondary" style="padding:0;">
            <div id="nav" class="sticky-top border-bottom border-secondary" >
                <!--to be displayed only on mobile-->
                <div class="dropdown" id="dropdown-col1-mobile" >
                    <img id="imgdrpdwn-mobile" onclick="showDropdownContent();" src="{{asset('storage/'.Auth::User()->avatar.'')}}" class="rounded-circle" alt="Cinque Terre" style="width: 3.5em;height: 3.3em;">
                    <div class="dropdown-content" id="drpdwncntn" >
                        <div id="banners-parent" class="d-flex flex-column justify-content-center" style=" height:100vh;position: relative ">
                            <div id="accnt-info-banner" class="display: flex;flex-direction:column;" style="position: fixed;top: 0; margin:1em 0;">
                                <div style="font-size: larger;color:white;font-weight: bold; margin-left:1em; display: flex;justify-content: space-between;">Account info <span style="margin-right:0.5em;" onclick="closeDropdown()"><i class="fa fa-times" aria-hidden="true"></i></span></div>
                                <hr style="height:1px; width:100%;">
                            </div>
                            <div style="display: flex;flex-direction:column; height:83%; margin-left:1em;margin-top: 2em;">
                                <div class="d-flex flex-column " style='margin-bottom: 0.7em; align-items: center;'>
                                    <img src="{{asset('storage/'.Auth::User()->avatar.'')}}" class="rounded-circle" alt="Cinque Terre" style="margin-bottom: 0.2em; width: 3.5em;height: 3.3em;">
                                    <div style="margin-bottom: 0.2em; color:white; width:fit-content;">{{ Auth::User()->name }}</div>
                                    <div style="color:#AAB8C2; width:fit-content;"> {{Auth::User()->username}}</div>
                                </div>
                                <div style="margin-bottom: 1em;"><span id="nb-tweets">500</span> tweets</div>
                                <div style="margin-bottom: 1em;">Membre since <span id="membre-year">2000</span></div>
                                <div style="margin-bottom: 1em;"><a href="/home"><i class="fa fa-home" aria-hidden="true"></i> Home</a></div>
                                <div style="margin-bottom: 1em;"><a href="{{url('/profile/'.Auth::User()->id.'')}}"><i class="fa fa-user" aria-hidden="true"></i> Profile</a></div>
                                <div><a href="{{ route('logout') }}"><i class="fa fa-sign-out" aria-hidden="true"></i> Log-out</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end of mobile feature-->
                <div  id="home-nav"><a href="#" ><Strong>Home</strong></a></div>
            </div>
            <form id="tweetForm" action="{{url('tweet/store')}}" method="POST">
                @csrf
                <div class="whatsup">
                    <div class="avatar" style="padding: 1em;">
                        <?php
                        $str = Auth::User()->avatar;
                        if(strpos($str, 'storage')){
                        ?>
                            <img src="{{asset(''.Auth::User()->avatar.'')}}" class="rounded-circle" alt="Cinque Terre" style="width: 3.5em;height: 3.3em;">
                        <?php
                        }else{
                        ?>
                            <img src="{{asset('storage/'.Auth::User()->avatar.'')}}" class="rounded-circle" alt="Cinque Terre" style="width: 3.5em;height: 3.3em;">
                        <?php
                        }
                        ?>
                    </div>
                    <div id="parentText" style="padding: 1em;width:100%;">
                        <textarea  name="text" id="text" placeholder="What's happening?" style="color:white;"></textarea>
                        <input type="hidden" id="tags" class="ui-autocomplete-input" autocomplete="off">
                    </div>
                </div>
                <button class="rounded-pill btn btn-primary" id="btntweet" type="submit" style="float:right;margin-right: 1em;" disabled>Tweet</button><br><br>
            </form>
            <div class="border-bottom-0 border border-secondary" style="padding:0; border-width: 0.7em!important;"></div>

            <!--tweets-->
            <div class="tweet">
            <div class="infinite-scroll">
                @foreach($results as $result)
                @if($result->isRetweet == true)
                <div class="continer" style="display:flex; flex-direction:column;margin: 1em 0 0 1em;">
                    <div><i class="fa fa-retweet" aria-hidden="true"></i> {{$result->user->name}} Retweeted</div>
                    <div class="tweet-container" style="padding: 1em;"> 
                        <div class="avatar-tweeto">
                        <?php
                        $str = $result->user->avatar;
                        if(strpos($str, 'storage')){
                        ?>
                            <img src="{{asset(''.$result->user->avatar.'')}}" class="rounded-circle" alt="Cinque Terre" style="width: 3.5em;height: 3.3em;">
                        <?php
                        }else{
                        ?>
                            <img src="{{asset('storage/'.$result->user->avatar.'')}}" class="rounded-circle" alt="Cinque Terre" style="width: 3.5em;height: 3.3em;">
                        <?php
                        }
                        ?>
                        </div>
                        <div class="tweet-body">
                            <span style="color:white;" ><a href="{{url('/profile/'.$result->user->id.'')}}">{{ $result->user->name }}</a></span><span style="color:#AAB8C2;"> <a href="{{url('/profile/'.$result->user->id.'')}}">{{"@".$result->user->username}} </a><span style="font-size: smaller;">{{ $result->created_at->diffForHumans() }}</span></span>
                            <span onclick="deleteTweet({{$result->id}});" style="float:right; cursor: pointer;margin-right: 0.5em;"><i class="fa fa-trash-o" aria-hidden="true"></i></span>
                                <div class="tweet-text">
                                    <span style="color:white;">{{$result->text}}</span>
                                    <div class="tweet-interaction">
                                        <span style="cursor: pointer;" data-toggle="modal" data-id="{{$result->id}}" class="open-modal" onClick='setFocus();'><i class="fa fa-comment-o" aria-hidden="true"></i></span>
                                        <span style="cursor: pointer; color:green;"><i  class="fa fa-retweet" aria-hidden="true"></i></span>
                                        <span><input class="heart" id="{{$result->created_at}}" type="checkbox" /><label for="{{$result->created_at}}"><i class="fa fa-heart-o" aria-hidden="true" onclick="addLike({{$result->id}});" style="cursor: pointer;"></i></label> <span id="{{$result->id}}">
                                        @if($result->likes > 0)
                                        {{$result->likes}}
                                        @endif
                                        </span></span>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                @else
                <div class="tweet-container" style="padding: 1em;"> 
                    <div class="avatar-tweeto">
                    <?php
                    $str = $result->user->avatar;
                    if(strpos($str, 'storage')){
                    ?>
                        <img src="{{asset(''.$result->user->avatar.'')}}" class="rounded-circle" alt="Cinque Terre" style="width: 3.5em;height: 3.3em;">
                    <?php
                    }else{
                    ?>
                        <img src="{{asset('storage/'.$result->user->avatar.'')}}" class="rounded-circle" alt="Cinque Terre" style="width: 3.5em;height: 3.3em;">
                    <?php
                    }
                    ?>
                    </div>
                    <div class="tweet-body">
                        <span style="color:white;" ><a href="{{url('/profile/'.$result->user->id.'')}}">{{ $result->user->name }}</a></span><span style="color:#AAB8C2;"> <a href="{{url('/profile/'.$result->user->id.'')}}">{{"@".$result->user->username}} </a><span style="font-size: smaller;">{{ $result->created_at->diffForHumans() }}</span></span>
                        @if($result->user->username == Auth::User()->username)
                        <span onclick="deleteTweet({{$result->id}});" style="float:right; cursor: pointer;margin-right: 0.5em;"><i class="fa fa-trash-o" aria-hidden="true"></i></span>
                        @endif    
                            <div class="tweet-text">
                                <span style="color:white;">{{$result->text}}</span>
                                <div class="tweet-interaction">
                                    <span style="cursor: pointer;" data-toggle="modal" data-id="{{$result->id}}" class="open-modal" onClick='setFocus();'><i class="fa fa-comment-o" aria-hidden="true"></i></span>
                            @can('create', App\Tweet::class)
                                @if($result->retweets ==null)
                                    <span onclick="addRetweet({{Auth::User()->id}}, {{$result->id}});" style="cursor: pointer;"><i class="fa fa-retweet" aria-hidden="true"></i></span>
                                @elseif($result->retweets != null || $result->retweets >0 )
                                    <span onclick="addRetweet({{Auth::User()->id}}, {{$result->id}});" style="cursor: pointer;"><i class="fa fa-retweet" aria-hidden="true"></i></span>
                                @endif
                            @endcan
                                    <span><input class="heart" id="{{$result->created_at}}" type="checkbox" /><label for="{{$result->created_at}}"><i class="fa fa-heart-o" aria-hidden="true" onclick="addLike({{$result->id}});" style="cursor: pointer;"></i></label> <span id="{{$result->id}}">
                                    @if($result->likes > 0)
                                    {{$result->likes}}
                                    @endif
                                    </span></span>
                                </div>
                            </div>
                    </div>
                </div>
                @endif
               @foreach($result->replies as $reply)
               <div class="vertical-ligne"></div>
                <!--the replys-->
                <div class="tweet-container" style="padding: 1em;"> 
                    <div class="avatar-tweeto">
                    <?php
                    $str = $reply->user->avatar;
                    if(strpos($str, 'storage')){
                    ?>
                        <img src="{{asset(''.$reply->user->avatar.'')}}" class="rounded-circle" alt="Cinque Terre" style="width: 3.5em;height: 3.3em;">
                    <?php
                    }else{
                    ?>
                        <img src="{{asset('storage/'.$reply->user->avatar.'')}}" class="rounded-circle" alt="Cinque Terre" style="width: 3.5em;height: 3.3em;">
                    <?php
                    }
                    ?>
                    </div>
                    <div class="tweet-body">
                        <span style="color:white;"><a href="{{url('/profile/'.$reply->user->id.'')}}">{{ $reply->user->name }}</a></span><span style="color:#AAB8C2;"> <a href="{{url('/profile/'.$reply->user->id.'')}}">{{"@".$reply->user->username}}</a> <span style="font-size: smaller;">{{ $reply->created_at->diffForHumans() }}</span></span>
                        @if($reply->user->username == Auth::User()->username)
                        <span onclick="deleteTweet({{$reply->id}});" style="float:right; cursor: pointer;margin-right: 0.5em;"><i class="fa fa-trash-o" aria-hidden="true"></i></span>
                        @endif    
                            <div class="tweet-text">
                                <span style="color:white;">{{$reply->text}}</span>
                                <div class="tweet-interaction">
                                    <span style="cursor: pointer;" data-toggle="modal" data-id="{{$result->id}}" class="open-modal" onClick='setFocus();'><i class="fa fa-comment-o" aria-hidden="true"></i></span>
                            @can('create', App\Tweet::class)
                                @if($reply->retweets ==null)
                                    <span onclick="addRetweet(Auth::User()->username, {{$reply->id}});" style="cursor: pointer;"><i class="fa fa-retweet" aria-hidden="true"></i></span>
                                @elseif($reply->retweets != null || $reply->retweets >0 )
                                    <span onclick="addRetweet(Auth::User()->username, {{$reply->id}});" style="cursor: pointer;"><i class="fa fa-retweet" aria-hidden="true"></i></span>
                                @endif
                            @endcan
                            <span><input class="heart" id="{{$reply->created_at}}" type="checkbox" /><label for="{{$reply->created_at}}"><i class="fa fa-heart-o" onclick="addLike({{$reply->id}});" aria-hidden="true" style="cursor: pointer;"></i></label> <span id="{{$reply->id}}">
                                    @if($reply->likes > 0)
                                    {{$reply->likes}}
                                    @endif
                                    </span></span>
                                </div>
                            </div>
                    </div>
                </div>
                @endforeach
                <div class="border-bottom-0 border border-secondary" style="padding:0;"></div>
                @endforeach
                {{$results->links("vendor/pagination/default")}}
                </div>
            </div>
            <!--to be displayed only on mobile-->
            <div id="mobileTweetBtn" style="position: fixed;bottom: 2em;left: 78%;">
                <button class="btn btn-primary btn-circle" data-toggle="modal" data-target="#myModal">Tweet</button>
            </div>
            <!--end of mobile feature-->
        </div>
        <div id="right" class="col">
            <p>There are no news here, just corona TIME !</p>
        </div>
    </div>
</div>
<!-- tweet Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="background-color:#141f27">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
            <form action="{{url('tweet/store')}}" method="POST">
                @csrf
                <div class="whatsup">
                    <div class="avatar">
                        <img src="{{asset('storage/'.Auth::User()->avatar.'')}}" class="rounded-circle" alt="Cinque Terre" style="width: 3.5em;height: 3.3em;">
                    </div>
                    <div style="padding: 1em;width:100%;">
                        <textarea name="text" id="text1" placeholder="What's happening?" style="color:white;"></textarea>
                    </div>
                </div>
                <button class="btn btn-primary" type="submit" id="btntweet1" style="float:right;margin-right: 1em;" disabled>Tweet</button><br><br>
            </form>
        </div>
    </div>

  </div>
</div>

<!--modal to reply-->
<div id="reply" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="background-color:#141f27">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
            <form action="{{url('tweet/store')}}" method="POST" name="reply-form">
                @csrf
                <div id="tweetContainer">
                </div>

                
                <button class="btn btn-primary" type="submit" style="float:right;margin-right: 1em;">Reply</button><br><br>
            </form>
        </div>
    </div>

  </div>
</div>
<script type="text/javascript">
   $(document).on("click", ".open-modal", function () {
        var id = this.getAttribute('data-id');
        $.ajax({
            type : 'get',
            headers: {'X-CSRF-Token': '{{ csrf_token() }}',},
            url : 'tweet/comment/', //Here you will fetch records 
            datatype: 'JSON',
            data :  'id='+ id, //Pass $id
            success : function(data){
                var resultat =  JSON.parse(data);
                var texte = "<div class='tweet-container' style='padding: 1em;'><div class='avatar-tweeto'><img src='storage/"+resultat[0].avatar+"' class='rounded-circle' alt='Cinque Terre' style='width: 3.5em;height: 3.3em;'></div><div class='tweet-body'><span style='color:white;'>"+resultat[0].name+"</span><span style='color:#AAB8C2;'> @"+resultat[0].username+"</span><div class='tweet-text'><span style='color:white;'>"+resultat[0].text+"</span><br><span>Replying to<span style='color:#1DA1F2;'> @"+resultat[0].username+"</span></span></div></div></div><div id='tweetReplys'></div>";
                texte += "<div class='whatsup'><div class='avatar' style='padding: 1em;'><img src='{{asset('storage/'.Auth::User()->avatar.'')}}' class='rounded-circle' alt='Cinque Terre' style='width: 3.5em;height: 3.3em;'></div><div style='padding: 1em;width:100%;'><textarea name='text' id='reply-area' placeholder='Tweet your reply' style='color:white;'></textarea><input type='hidden' name='to_tweet_id' value='"+id+"'></input></div></div>";
                $("#tweetContainer").html(texte);//Show fetched data from database
                
                //second ajax call:
                $.ajax({
                    type : 'get',
                    headers: {'X-CSRF-Token': '{{ csrf_token() }}',},
                    url : 'tweet/comment/showReplys', //Here you will fetch records 
                    datatype: 'JSON',
                    data :  'id='+ id, //Pass $id
                    success : function(data){
                        var resultat_Replys =  JSON.parse(data);
                        var contenu ="";
                        for(var i=0; i<resultat_Replys.length; i++){
                            contenu += "<div class='tweet-container' style='padding: 1em;'><div class='avatar-tweeto'><img src='storage/"+resultat_Replys[i].avatar+"' class='rounded-circle' alt='Cinque Terre' style='width: 3.5em;height: 3.3em;'></div><div class='tweet-body'><span style='color:white;'>"+resultat_Replys[i].name+"</span><span style='color:#AAB8C2;'> @"+resultat_Replys[i].username+"</span><div class='tweet-text'><span style='color:white;'>"+resultat_Replys[i].text+"</span><br><span>Replying to<span style='color:#1DA1F2;'> @"+resultat_Replys[i].username+"</span></span></div></div></div>";
                        }
                    $("#tweetReplys").html(contenu);//Show fetched data from database
                    }
                }); 

            }
        }); 
        $('#reply').modal('show');
    });
    


//to make textarea focused apon load/opening modal
    function setFocus(elem){
        document.reply-form.reply-area.focus();
    }


//to add likes => increment
function addLike(tweetId){
    $.ajax({
                type : 'post',
                headers: {'X-CSRF-Token': '{{ csrf_token() }}',},
                url : 'tweet/likes', //Here you will fetch records 
                datatype: 'JSON',
                data :  'id='+ tweetId, //Pass $id
                success : function(data){
                    console.log("nb tweets: "+data);
                    //location.reload();
                    $('#'+tweetId).html(data);
                }
            }); 
            console.log("tweetId: "+tweetId);
}
//to add retweets => increment
function addRetweet(userId, tweetId){
    console.log("userId: "+userId);
    console.log("tweetId: "+tweetId);
    $.ajax({
                type : 'post',
                headers: {'X-CSRF-Token': '{{ csrf_token() }}',},
                url : 'tweet/retweets', //Here you will fetch records 
                datatype: 'JSON',
                data :  'id='+ userId, //Pass $id
                data: {userId: userId, tweetId: tweetId},
                success : function(data){
                    console.log(data);
                }
            }); 
            console.log(userId);
}

//delete tweet
function deleteTweet(tweetId){
    $.ajax({
                type : 'post',
                headers: {'X-CSRF-Token': '{{ csrf_token() }}',},
                url : 'tweet/delete', //Here you will fetch records 
                datatype: 'JSON',
                data :  'id='+ tweetId, //Pass $id
                success : function(data){
                    console.log(data);
                    location.reload();
                }
            }); 
            console.log(tweetId);
}
</script>


<script>
    $(document).ready(function(){
        $('#text').keyup(function(){
            document.getElementById("btntweet").disabled = false;
            var text = document.getElementById('text').value;
            if(text == ""){
                document.getElementById("btntweet").disabled = true;
            }
            var array=[];
            $.ajax({
                type : 'get',
                headers: {'X-CSRF-Token': '{{ csrf_token() }}',},
                url : 'AutoCompleteUsers/index', //Here you will fetch records 
                datatype: 'JSON',
                success : function(data){
                    console.log(data);
                    var count = Object.keys(data).length;
                    for(var i=0; i<=count; i++){
                        if(data[i] != null){
                            array[i] = data[i].username;
                        }else{
                            break;
                        }
                    }
                    console.log(array);
                    $('#text').atwho({
                        at: "@",
                        data:array
                    })
                }
            }); 
        })
    })
    $(document).ready(function(){
        $('#text1').keyup(function(){
            document.getElementById("btntweet1").disabled = false;
            var text = document.getElementById('text1').value;
            if(text == ""){
                document.getElementById("btntweet1").disabled = true;
            }
            var array=[];
            $.ajax({
                type : 'get',
                headers: {'X-CSRF-Token': '{{ csrf_token() }}',},
                url : 'AutoCompleteUsers/index', //Here you will fetch records 
                datatype: 'JSON',
                success : function(data){
                    console.log(data);
                    var count = Object.keys(data).length;
                    for(var i=0; i<=count; i++){
                        if(data[i] != null){
                            array[i] = data[i].username;
                        }else{
                            break;
                        }
                    }
                    console.log(array);
                    $('#text1').atwho({
                        at: "@",
                        data:array
                    })
                }
            }); 
        })
    })
  
//fill users handle after @ in textarea

   /* $(document).ready(function(){
        $('#text').keyup(function(){
            document.getElementById("btntweet").disabled = false;
            var text = document.getElementById('text').value;
            if(text == ""){
                document.getElementById("btntweet").disabled = true;
            }
            console.log(text);
            if(text.includes("@")){
                console.log(text);
                var array = text.split("@");
                var username = array[1];
                $.ajax({
                        type : 'get',
                        headers: {'X-CSRF-Token': '{{ csrf_token() }}',},
                        url : 'AutoCompleteUsers/index', //Here you will fetch records 
                        datatype: 'JSON',
                        data: 'username=' +username,
                        success : function(data){
                            console.log(data);
                            var contenu="";
                            if(document.getElementById('dropdwn') != null){
                                document.getElementById('dropdwn').innerHTML ="";
                            }
                            contenu += "<div id='dropdwn' style='color:red;' class='dropdown-menu'>";
                            var arr ="";
                            for(var i=0; i<5; i++){
                                if(data[i] != null){
                                    contenu +="<a style='color:Red;' class='dropdown-item'>"+data[i].username+"</a>";
                                    console.log(data[i].username);
                                }else{
                                    break;
                                }
                            }
                            contenu+="</div>"
                            document.getElementById("tags").innerHTML+=contenu;
                        }
                }); 
            }
        })
    })*/


    
</script>


<script type="text/javascript">
//mobile dropdown menu
function showDropdownContent(){
    document.getElementById("imgdrpdwn-mobile").style.display="none";
    document.getElementById("home-nav").style.display="none";
    document.getElementById("drpdwncntn").style.display="block";
    document.getElementById("drpdwncntn").style.backgroundColor="#141f27";
    document.getElementById("dropdown-col1-mobile").style.width="45%";
    document.getElementById("nav").style.padding="0.2em";
    var parents_width = $("#banners-parent").width();
    $("#accnt-info-banner").width(parents_width);
   //ajax call to retrieve some statistics
   //numbre of tweets
   $.ajax({
        type : 'post',
        headers: {'X-CSRF-Token': '{{ csrf_token() }}',},
        url : 'tweet/statistics', //Here you will fetch records 
        datatype: 'JSON',
        data :  'id='+ {{Auth::User()->id}}, //Pass $id
        success : function(data){
            var res =  JSON.parse(data);
            console.log(res);
            var year = res[1].split('-');
            document.getElementById("nb-tweets").innerHTML = res[0];
            document.getElementById("membre-year").innerHTML = year[0] ;
        }
    }); 
}

function closeDropdown(){
    document.getElementById("drpdwncntn").style.display="none";
    document.getElementById("imgdrpdwn-mobile").style.display="block";
    document.getElementById("home-nav").style.display="inline-block";
    document.getElementById("dropdown-col1-mobile").style.width="";
    document.getElementById("nav").style.padding="20px";
    //var parents_width = $("#banners-parent").width();
    //$("#accnt-info-banner").width(parents_width);
}
</script>
@endsection
