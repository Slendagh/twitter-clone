@component('mail::message')
# Welcome to Twitter

Please verify your email by clicking the button below.

@component('mail::button', ['url' => ''])
Verify email
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
