<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Twitter') }}</title>

    
    <!--jquery-->
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <!--test github stuff for autocomplete-->
    <script src="{{ asset('js/jquery.caret.js')}}"></script>
    <script src="{{ asset('js/jquery.atwho.js')}}"></script>
    <!--end autocomplete stuff-->
    <script src="{{asset('js/jquery.jscroll.min.js')}}"></script>
    
    <!-- Scripts--> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>-->
    <script src="{{asset('js/script.js')}}"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('css/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <!--autocomplete-->
    <link href="{{ asset('css/jquery.atwho.css')}}" rel="stylesheet">
    <!--<link href="{{ asset('css/app.css') }}" rel="stylesheet">-->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet">



    
</head>
<body >
  
    <div id="app" class="h-100" >
     

        <main class="h-100" >
            @yield('content')
        </main>
    </div>
</body>
</html>
