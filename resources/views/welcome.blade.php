@extends ('layouts.app')

@section ('content')
    <div class="d-flex flex-column justify-content-center h-100 h1 text-body">
        <img class="twitter-logo align-self-center mb-5" src="https://static01.nyt.com/images/2014/08/10/magazine/10wmt/10wmt-superJumbo-v4.jpg"/>
        <div class="align-self-center mb-4">See what's happening in the world right now</div>
        <div class="align-self-center mb-4">Join Twitter today.</div>
        <button class="btn btn-primary align-self-center rounded-pill mb-2" style="width: 20em;"><a class="nav-link" href="{{ route('register') }}">{{ __('Sign up') }}</a></button>
        <button class="btn align-self-center rounded-pill border border-primary mb-2" style="width: 20em;" ><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></button>
        <button class="btn align-self-center rounded-pill border btn-secondary btn-block" style="width: 20em;"><a href="/sign-in/github">Log in with Github</a></button>
    </div>
        
@endsection

                
            