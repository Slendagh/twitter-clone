<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    protected $attributes = [
        'likes' => 0,
        'to_tweet_id' =>0,
        'retweets_id' => 0,
    ];

    public function replies(){
        return $this->hasMany('App\Tweet', 'to_tweet_id');
    }

    public function retweets(){
        return $this->hasMany('App\Tweet', 'retweets_id');
    }
    
    public function user(){
        return $this->belongsTo('App\User');
    }
}
