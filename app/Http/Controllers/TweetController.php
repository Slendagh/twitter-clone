<?php

namespace App\Http\Controllers;

use App\Tweet;
use App\User;
use Illuminate\Http\Request;
use Auth;
use redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Date;
use App\Notifications\UsernameTagged;

class TweetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$results = DB::table('users')->join('tweets', 'users.id', 'user_id')->orderBy('tweets.created_at', 'desc')->limit(50)->get();
        //problem with relation retweets.... retweets array empty...
        $results = Tweet::where('retweets_id','>', 0)->with('retweets')->with('retweets.user')->where('to_tweet_id', '=', 0)->with('replies')->with('user')->with('replies.user')->orderBy('tweets.created_at', 'desc')->get();
        //return view('home', ['results'=>$results]);
     dd($results);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tweet = new Tweet;
        $tweet->text = $request->text;
        $tweet->user_id = Auth::user()->id;
        $tweet->created_at = Date::now(); 
        if($request->to_tweet_id){
            $tweet->to_tweet_id = $request->to_tweet_id;
        }
        $tweet->save();

        //to notify
        if(str_contains($request->text,"@")){
            $first_array = explode("@", $request->text);
            $array = explode(' ', $first_array[1]);
            $username = trim($array[0]);
            $res = User::where('username', '=', $username)->get();
            //dd($res, $username);
            $res[0]->notify(new UsernameTagged());
            
        }
        
        return redirect()->action('TweetController@index');
    }

    public function storeLikes(Request $request){
        $res = Tweet::where('id', '=', $request->id)->increment('likes');
        $resultat = Tweet::where('id', $request->id)->get();
        echo $resultat[0]->likes;
    }

    public function storeRetweets(Request $request){
        $this->authorize('create', Tweet::class);//to authorize only superUsers to retweets
        $res = Tweet::where('id', '=', $request->tweetId)->get();
        $tweet = new Tweet;
        $tweet->text = $res[0]->text;
        $tweet->user_id = $request->userId;
        $tweet->created_at = Date::now();
        $tweet->likes = $res[0]->likes;
        $tweet->to_tweet_id = $res[0]->to_tweet_id;
        $tweet->retweets_id = $request->tweetId;
        $tweet->isRetweet = true;
        $tweet->save();
        $nbRetweets = Tweet::where('retweets_id', '=', $request->tweetId)->count();
        echo $nbRetweets;
    }

    public function statistics(Request $request){
        $nbTweet = Tweet::where('user_id', '=', $request->id)->count();
        $user = User::where('id', '=', $request->id)->get();
        $year = $user[0]->email_verified_at;
        $data[0] = $nbTweet;
        $data[1] = $year;
        echo json_encode($data);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Tweet  $tweet
     * @return \Illuminate\Http\Response
     */
    public function showTweet(Request $request)
    {
            $resultat = DB::table('users')->join('tweets', 'users.id', 'user_id')->where('tweets.id', $request->id)->orderBy('tweets.created_at', 'desc')->get();
            echo $resultat;
         
    }

    public function showReplys(Request $request){
        $resultat = DB::table('users')->join('tweets', 'users.id', 'user_id')->where('tweets.to_tweet_id', $request->id)->orderBy('tweets.created_at', 'asc')->get();
        echo $resultat;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tweet  $tweet
     * @return \Illuminate\Http\Response
     */
    public function edit(Tweet $tweet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tweet  $tweet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tweet $tweet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tweet  $tweet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $res=Tweet::find($request->id);
        if($res){
            $res->delete();
        }
        echo $res;
    }

    public function showProfile(Request $request){
        $results = Tweet::where('user_id', '=', $request->id)->with('user')->get();
        //dd($results);
        return view('profile', ['results'=>$results]);
    }
}
