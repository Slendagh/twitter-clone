<?php

namespace App\Exports;

use App\Tweet;
use Maatwebsite\Excel\Concerns\FromCollection;

class TweetsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Tweet::all();
    }
}
