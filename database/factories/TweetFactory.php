<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Tweet;
use Faker\Generator as Faker;

$factory->define(Tweet::class, function (Faker $faker) {
    $userId = DB::table('users')->pluck('id')->toArray();
    return [
        'text' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'user_id' => $faker->randomElement($userId),
        'likes' => 0,
        'retweets_id' => 0,
        'isRetweet' => false,
        'by_user_id' => null,
        'created_at' => $faker->dateTime($max = 'now', $timezone = null),
        'updated_at' => $faker->dateTime($max = 'now', $timezone = null),
    ];
});
