<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //without factories
        $this->call([UserSeeder::class, TweetSeeder::class]);
        //with factories
        /*factory(App\User::class, 50)->create()->each(function ($user) {
            $user->tweets()->save(factory(App\Tweet::class)->make());
        });*/
    }
}
