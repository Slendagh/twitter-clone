<?php
require_once 'vendor/autoload.php';

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $filePath = storage_path('avatars');
        DB::table('users')->insert([
            'name' => $faker->name,
            'username' =>'@'.$faker->username,
            'email' => $faker->email,
            'password' => $faker->password,
            'avatar' => 'avatars/'.$faker->image('public/storage/avatars',400,300, null, false),
            'created_at' => $faker->dateTime($max = 'now', $timezone = null),
            'updated_at' => $faker->dateTime($max = 'now', $timezone = null),
            'superUser' => $faker->boolean(),
        ]);
    }
}
