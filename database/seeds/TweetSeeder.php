<?php
require_once 'vendor/autoload.php';
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TweetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userId = DB::table('users')->pluck('id')->toArray();
        $faker = Faker\Factory::create();
        DB::table('tweets')->insert([
            'text' => $faker->realText($maxNbChars = 200, $indexSize = 2),
            'user_id' => $faker->randomElement($userId),
            'likes' => 0,
            'retweets_id' => 0,
            'isRetweet' => false,
            'to_tweet_id' => 0,
            'by_user_id' => null,
            'created_at' => $faker->dateTime($max = 'now', $timezone = null),
            'updated_at' => $faker->dateTime($max = 'now', $timezone = null),
        ]);
    }
}
